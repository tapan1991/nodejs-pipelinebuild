
/**
* TODO: URL must come from constant config file

*/


angular.module('PacketoApp').factory('AppService', ['$http', '$location', '$rootScope', '$cookieStore',
    function ($http, $location, $rootScope, $cookieStore) {
     var url = 'http://localhost:8090';
//    var url = 'http://staging.paketo.io';
        return {
            saveApp: function(data){
            	return $http({
            		method : 'POST',
                    data: JSON.stringify(data),
            		url : url+'/api/pipeline/save',
					headers: {'Content-Type': 'application/json'},
            		timeout:60000
            	});
            }

        }
    }
]);


