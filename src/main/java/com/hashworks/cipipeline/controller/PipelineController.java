package com.hashworks.cipipeline.controller;

import com.hashworks.cipipeline.service.PipelineService;
import com.hashworks.cipipeline.utils.ResponseJsonUtil;
import org.apache.catalina.Pipeline;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;


@RequestMapping(value = "/api/pipeline/")
@RestController
public class PipelineController {


    @Autowired
    PipelineService pipelineService;

    @Autowired
    ResponseJsonUtil responseJsonUtil;

    @RequestMapping(value = "save", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> saveJobStatus(@RequestBody Map requestJson) {
        Map responseMap = new HashMap();
        HttpStatus status = HttpStatus.OK;
        System.out.print("request json from jenkins "+requestJson);
        responseMap=responseJsonUtil.getResponseJson(200, pipelineService.buildPipeline(requestJson));
        return new ResponseEntity<Object>(responseMap, HttpStatus.OK);
    }
}