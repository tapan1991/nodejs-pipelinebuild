package com.hashworks.cipipeline.utils;

import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

@Component
public class ResponseJsonUtil {



	//TODO: RESPONSE JSON FORMAT SHOULD BE {"status":"Success",data:{ } }
	public  Map<?, ?> getResponseJson(int status, List respJson){
		Map responseBuilder = new HashMap();
		responseBuilder.put("status", status);
		responseBuilder.put("data", respJson);
        return responseBuilder;
	}
	public  Map<?, ?> getResponseJson(int status, String respJson){
		Map responseBuilder = new HashMap();
		responseBuilder.put("status", status);
		responseBuilder.put("data", respJson);
        return responseBuilder;
	}


    public Map getResponseJson(int status, Set<String> respJson) {
		Map responseBuilder = new HashMap();
		responseBuilder.put("status", status);
		responseBuilder.put("data", respJson);
		return responseBuilder;
    }

	public Map getResponseJson(int status, Object a) {
		Map responseBuilder = new HashMap();
		responseBuilder.put("status", status);
		responseBuilder.put("data", a);
		return responseBuilder;
	}


}
