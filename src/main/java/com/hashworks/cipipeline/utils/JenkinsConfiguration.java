/**
 * 
 */
package com.hashworks.cipipeline.utils;

import com.offbytwo.jenkins.JenkinsServer;
import org.springframework.stereotype.Component;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLSocketFactory;
import java.net.URI;
import java.net.URISyntaxException;


/**
 * @author Paulvy_Varghese
 *
 */
@Component
public class JenkinsConfiguration {
	int port = 443;
	 
	String hostname = "https://ci.hashworks.co";
	 
	SSLSocketFactory factory = HttpsURLConnection
	.getDefaultSSLSocketFactory();
	
	public JenkinsServer getJenkinsConnection() throws URISyntaxException
	{
		JenkinsServer jenkins = new JenkinsServer(new URI(hostname), "hashworksadmin", "Hashworks@123#");
	    return jenkins;
	}

}