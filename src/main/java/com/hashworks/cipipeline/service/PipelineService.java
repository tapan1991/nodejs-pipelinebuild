package com.hashworks.cipipeline.service;


import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectReader;
import com.hashworks.cipipeline.models.AppEnv;
import com.hashworks.cipipeline.models.AppPipelineConfig;
import com.hashworks.cipipeline.repository.AppEnvRepository;
import com.hashworks.cipipeline.repository.AppPipeLineConfigRepository;
import com.hashworks.cipipeline.utils.JenkinsConfiguration;
import com.offbytwo.jenkins.JenkinsServer;
import org.apache.commons.lang.ObjectUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Array;
import java.net.URISyntaxException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

@Service
public class PipelineService {


    /**
    *  {"appName":"test","repoUrl":"","branch":"","appEnv":"spring/node/angular"}
    */


    /**
     * Json format to pass input from front-end
     * {
     * "RepositoryUrl":"https://tapan2609@bitbucket.org/tapan2609/node-js-jenkins-integration.git",
     * "Branch":"master"
     * }
     */
    @Autowired
    private  AppEnvRepository appEnvRepository;

    @Autowired
    private AppPipeLineConfigRepository appPipeLineConfigRepository;

    private static String pipelineXml="pipeline-xml";
    private static String pipelineName = "OneClickDeployment";
    private static String NodeJs = "Node-Js";
    private static String SpringBoot = "Spring-Boot";
    private static String AngularJs = "Angular-Js";
    private static String Build = "Build";
    private static String SCA = "SCA";
    private static String Deploy = "Deploy";
    private static String DIT = "DIT";
    private static String pipeline = "pipeline";

    @Value("${ci.jenkins.url}")
    private String jenkinsCiUrl;

    @Value("${ci.jenkins.credentials}")
    private String ciJenkinsCredentials;

    @Value("${ci.jenkins.token}")
    private String ciJenkinsToken;

    @Value("${ci.jenkins.credentialsId}")
    private String CredentialsId;

    public String buildPipeline(Map responseMap){

        try {
            JenkinsServer jenkinsServer = getJenkinCon();
            AppEnv appEnv = appEnvRepository.findByName(responseMap.get("appEnv").toString());
            AppPipelineConfig appPipelineConfig=appEnv.getAppPipelineConfig();
            String steps=appPipelineConfig.getSteps();


            if(appPipelineConfig.getFolderName().equals(NodeJs))
            {
                buildNodeJsPipe(appEnv,appPipelineConfig,responseMap);

            }

            else if(appPipelineConfig.getFolderName().equals(SpringBoot))
            {
                buildSpringBootJsPipe(appEnv,appPipelineConfig,responseMap);
            }

            else if(appPipelineConfig.getFolderName().equals(AngularJs))
            {
                buildAngularJsPipe(appEnv,appPipelineConfig,responseMap);
            }

            else
            {
                return "Undefined application.";
            }



        } catch (Exception e) {
            e.printStackTrace();
        }


        return "Pipeline Build Successfull";
    }

    private void buildAngularJsPipe(AppEnv appEnv, AppPipelineConfig appPipelineConfig, Map responseMap) {
    }

    private void buildSpringBootJsPipe(AppEnv appEnv, AppPipelineConfig appPipelineConfig, Map responseMap) {
    }

    private String buildNodeJsPipe(AppEnv appEnv, AppPipelineConfig appPipelineConfig, Map responseMap) throws IOException, URISyntaxException {

        try {
            ObjectMapper objectMapper = new ObjectMapper();
            String steps = appPipelineConfig.getSteps();
            JsonNode jsonNode = objectMapper.readTree(steps);

            String prevJobname = null;
            String FirstJobname = null;
            for (JsonNode node : jsonNode) {
                String readXml = readXmlPath(pipelineXml + File.separator + appPipelineConfig.getFolderName() + File.separator + node.textValue() + ".xml");
                String xmlString = null;
                String url = responseMap.get("repoUrl").toString();
                String branch = responseMap.get("branch").toString();
                if (node.asText().equals(Build)) {
                    xmlString = readXml.replace("@repoUrl", url).replace("@credId", CredentialsId).replace("@branch", branch);
                    prevJobname = node.asText();
                    FirstJobname = responseMap.get("appName") + "-" + node.asText();
                    createJenkinsJob(responseMap.get("appName").toString() + "-" + node.asText(), xmlString);

                } else if (node.asText().equals(SCA))

                {
                    xmlString = readXml.replace("@PreviousJob", responseMap.get("appName").toString() + "-" + prevJobname);
                    prevJobname = node.asText();
                    createJenkinsJob(responseMap.get("appName").toString() + "-" + node.asText(), xmlString);

                } else if (node.asText().equals(DIT)) {
                    xmlString = readXml.replace("@PreviousJob", responseMap.get("appName").toString() + "-" + prevJobname).replace("@FirstJob",responseMap.get("appName").toString()+"-"+Build);
                    prevJobname = node.asText();
                    createJenkinsJob(responseMap.get("appName").toString() + "-" + node.asText(), xmlString);


                } else if (node.asText().equals(Deploy)) {
                    xmlString = readXml.replace("@PreviousJob", responseMap.get("appName").toString() + "-" + prevJobname);
                    prevJobname = node.asText();
                    createJenkinsJob(responseMap.get("appName").toString() + "-" + node.asText(), xmlString);

                } else if (node.asText().equals(pipeline)) {
                    createPipelineView(appPipelineConfig, responseMap, FirstJobname, readXml);
                    startJenkinsJob(FirstJobname);

                } else {
                    return ("No Jobs has been created.");
                }


            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        return  "Pipeline Build created successfully.";

    }

    //This function is used to create pipeline view in jenkins
    private void createPipelineView(AppPipelineConfig appPipelineConfig,Map respMap, String firstJobname, String readXml) {
        String xmlString;
        xmlString = readXml.replace("@name",appPipelineConfig.getFolderName().toString()).replace("@FirstJob", firstJobname);
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders requestHeaders = new HttpHeaders();
        requestHeaders.setContentType(MediaType.APPLICATION_XML);

        getBasicAuth(requestHeaders);
        String ciHashWorks = jenkinsCiUrl+"/createView?name="+appPipelineConfig.getFolderName().toString()+"-"+respMap.get("appName").toString()+"-"+pipelineName;
        HttpEntity request = new HttpEntity(xmlString,requestHeaders);
        ResponseEntity<String> response=restTemplate.exchange(ciHashWorks, HttpMethod.POST,request,String.class);
    }

    //This function is used to get connection to jenkins
    public JenkinsServer getJenkinCon() throws URISyntaxException {
        JenkinsConfiguration jenkinsConfiguration = new JenkinsConfiguration();
        JenkinsServer jenkins = jenkinsConfiguration.getJenkinsConnection();
        return  jenkins;
    }

//This function is used to read xml path
    public String readXmlPath(String path) throws IOException {
        byte[] xmlData = Files.readAllBytes(new File(path).toPath());
        return new String(xmlData);
    }

//This function is used to create jenkins job
    public HttpStatus createJenkinsJob(String jobName, String xmlString) throws  IOException,URISyntaxException
    {
        getJenkinCon().createJob(jobName,xmlString);
        return HttpStatus.OK;
    }

//This function is used to get basic authorizartion header
    public void getBasicAuth(HttpHeaders requestHeaders) {
        byte[] encodedAuth = org.apache.commons.codec.binary.Base64.encodeBase64(ciJenkinsCredentials.getBytes(Charset.forName("US-ASCII")) );
        String authHeader = "Basic " + new String( encodedAuth );
        requestHeaders.set( "Authorization", authHeader );
    }

    //This is custom function is used to trigger jenkins job
    public void startJenkinsJob(String jenkinsJob)
    {

        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders requestHeaders = new HttpHeaders();
        requestHeaders.setContentType(MediaType.MULTIPART_FORM_DATA);
        getBasicAuth(requestHeaders);
        String ciHashWorks
                = jenkinsCiUrl + "/job/"+jenkinsJob+"/"+"build?token="+ciJenkinsToken;
        HttpEntity request = new HttpEntity(requestHeaders);
        ResponseEntity<String> response = restTemplate.exchange(ciHashWorks, HttpMethod.POST, request, String.class);


    }




}
