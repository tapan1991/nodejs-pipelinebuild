package com.hashworks.cipipeline;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CiPipelineApplication {


	public static void main(String[] args) {
		//Runtime.getRuntime().exec("mysqldump ci_pipeline_appenv.sql")
		SpringApplication.run(CiPipelineApplication.class, args);
	}
}
