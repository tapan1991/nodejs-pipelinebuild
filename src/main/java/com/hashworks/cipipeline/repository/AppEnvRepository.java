package com.hashworks.cipipeline.repository;

import com.hashworks.cipipeline.models.AppEnv;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AppEnvRepository extends JpaRepository<AppEnv, String> {

    AppEnv findByName(String appName);
}