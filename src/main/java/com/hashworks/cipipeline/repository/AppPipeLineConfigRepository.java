package com.hashworks.cipipeline.repository;

import com.hashworks.cipipeline.models.AppPipelineConfig;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AppPipeLineConfigRepository extends JpaRepository<AppPipelineConfig, Long> {

    List<AppPipelineConfig> findAll();
}