package com.hashworks.cipipeline.models;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name ="appenv")
public class AppEnv {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Getter
    @Setter
    private Long id;

    @Getter
    @Setter
    private String name;
    //feilds
    //id, name

    @Getter
    @Setter
    @OneToOne

    private AppPipelineConfig appPipelineConfig;

}
