package com.hashworks.cipipeline.models;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name ="app_pipeline_config")
public class AppPipelineConfig {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Getter
    @Setter
    private Long id;

    @Getter
    @Setter
    @OneToOne
    @JoinColumn(name = "appEnvId")
    private AppEnv appEnv;

    @Getter
    @Setter
    private String steps;

    @Getter
    @Setter
    private String folderName;




}
