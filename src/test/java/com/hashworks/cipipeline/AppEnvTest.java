package com.hashworks.cipipeline;


import com.hashworks.cipipeline.service.PipelineService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.net.URISyntaxException;

@RunWith(SpringRunner.class)
@SpringBootTest
public class AppEnvTest {

    @Autowired
    private PipelineService pipelineService;

    @Test
    public void test(){
        String appName = "Spring-Boot";

//        applicationService.createJenkinsJob();
//        credentialsService.saveCredToJenkins();
        try {
            pipelineService.getJenkinCon();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
    }

}
