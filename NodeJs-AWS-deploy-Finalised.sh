#!/bin/bash
#installing nodejs

sudo rm -rf *
#Enable ip forwarding in /etc/sysctl.conf
sed 's/^#net.ipv4.ip_forward=1/net.ipv4.ip_forward=1/g' /etc/sysctl.conf
sudo sysctl -p /etc/sysctl.conf

#set up forwarding from 80 to 8080:
sudo iptables -A PREROUTING -t nat -i eth0 -p tcp --dport 80 -j REDIRECT --to-port 8080

#open the Linux firewall to allow connections on port 80
sudo iptables -A INPUT -p tcp -m tcp --sport 80 -j ACCEPT
sudo iptables -A OUTPUT -p tcp -m tcp --dport 80 -j ACCEPT

sudo apt-get install unzip
sudo wget --auth-no-challenge --http-user=admin --http-password=32c7ef15ae284c05b23bbb77975c6189 https://ci.hashworks.co/job/NodeJs-UnitTest-Job/ws/workspace.zip
sudo unzip workspace.zip
cd workspace
sudo rm -rf node_modules
sudo apt-get install nodejs -y
sudo apt-get install npm -y
sudo npm init -y
sudo npm install --production
sudo npm install -g pm2@latest

#Start nodeJs app using forever
echo "starting the application"
pm2 start server.js
ps -e | grep node
exit
