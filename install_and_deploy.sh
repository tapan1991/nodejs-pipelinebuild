#!/bin/bash
#installing nodejs

#Update ubuntu package
sudo apt-get update

#Install 4.x version of nodejs using PPA
cd ~
sudo curl -sL https://deb.nodesource.com/setup_4.x -o nodesource_setup.sh
sudo bash nodesource_setup.sh
sudo apt-get install nodejs -y 
sudo apt-get install build-essential -y

#Change node path from "usr/bin/nodejs" to "usr/bin/node"
sudo ln -s /usr/bin/nodejs /usr/bin/node

#Install a node package manager ,express
sudo npm install express

#Enable ip forwarding in /etc/sysctl.conf
sed 's/^#net.ipv4.ip_forward=1/net.ipv4.ip_forward=1/g' /etc/sysctl.conf
sudo sysctl -p /etc/sysctl.conf

#set up forwarding from 80 to 8080:
sudo iptables -A PREROUTING -t nat -i eth0 -p tcp --dport 80 -j REDIRECT --to-port 8080

#open the Linux firewall to allow connections on port 80
sudo iptables -A INPUT -p tcp -m tcp --sport 80 -j ACCEPT
sudo iptables -A OUTPUT -p tcp -m tcp --dport 80 -j ACCEPT

#Install unzip package
sudo apt-get install unzip -y

#Download latest zip from jenkins
wget --auth-no-challenge --http-user=admin --http-password=32c7ef15ae284c05b23bbb77975c6189 https://ci.hashworks.co/job/NodeJs-UnitTest-Job/ws/workspace.zip

#Unzip workspace.zip
sudo unzip workspace.zip

#Remove pre downloaded node_modules from workspace
cd workspace
sudo rm -rf node_modules

#Initialize package.json
sudo npm init -y

#Install npm production version
sudo npm install --production

#Install npm - forever
sudo npm install -g forever

#Start nodeJs app using forever
echo "starting the application"
sudo forever start app.js
sleep 330s
echo "Application started successfully"
exit
