'use strict';

/*
*TODO: Create single error Page and Function to divert to error page
*/
var app =
    angular
        .module('PacketoApp', [
            'ngAnimate',
            'ngCookies',
            'ngResource',
            'ngRoute',
            'ngSanitize',
            'ngTouch',
            'ui.bootstrap',
            'ngMaterial',
            'ngTable',
            'angularjs-dropdown-multiselect',
            'angular-loading-bar',
            'angularUtils.directives.dirPagination',

        ]);

	app.config(function ($routeProvider) {
        $routeProvider
            .when('/', {
                templateUrl: 'app/views/AddApplication.html',
                controller: 'AddAppCtrl'
            })

            .otherwise({
                templateUrl: 'app/404.html'
            });
    });


app.config(['cfpLoadingBarProvider', function(cfpLoadingBarProvider) {
    cfpLoadingBarProvider.includeSpinner = false;
  }]);


