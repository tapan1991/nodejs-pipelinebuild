angular.module('PacketoApp').controller('AddAppCtrl', function (  $scope, $http, AppService,$location,$mdToast,$uibModal) {
    init();

    function init(){
        $scope.data={};
        $scope.appEnv=["Angular-Js","Node-Js","Spring-Boot"]
    }



    $scope.submit=function(){
     console.log($scope.data);
                AppService.saveApp($scope.data).success(function (data, status) {
                                       if (status == 500) {
                                           $location.url('/error');
                                       } else if (status == 404) {
                                           $location.url('/notFound')
                                       } else {
                                           $scope.appSave = data.data;
                                           console.log("app save ",data);
                                            $mdToast.show(
                                                       	      $mdToast.simple()
                                                       	        .textContent(data.data)
                                                       	        .position('Top Right')
                                                       	        .hideDelay(10000)
                                                       	    );
                                            $location.url('/');
                                       }
                                   }).error(function (error) {
                                       console.log(error);
                                   });

    }

   $scope.getCredentials=function(){
   $uibModal.open({
               animation: true,
               templateUrl: 'app/views/credential.html',
               size: 'lg',
               controller: 'CredCtrl',
               scope: $scope,
               resolve: {

               }
           });
   }


    });